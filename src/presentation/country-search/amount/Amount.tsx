import React, {useRef, useState} from 'react';
import {AmountStyled} from './Amount.styled';
import {useDispatch} from 'react-redux';
import {changeAmount} from './../../../redux/ducks/amount';
import {NumberFormatValues} from 'react-number-format';
import {CustomNumberFormatInput} from './../../../shared/text-input/TextInput';

const Amount = () => {
  const dispatch = useDispatch();
  const [amount, setAmount] = useState<number | undefined>(1);

  const timeoutRef = useRef<NodeJS.Timeout | undefined>(undefined);
  const inputChangeHandler = (values: NumberFormatValues): void => {
    const newValue = values.floatValue;
    setAmount(newValue);
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
    timeoutRef.current = setTimeout(() => {
      dispatch(changeAmount(newValue));
    }, 500);
  };

  return (
    <AmountStyled>
      <CustomNumberFormatInput
        data-cy="AmountInput"
        value={amount}
        thousandSeparator={true}
        icon={<span>kr</span>}
        onValueChange={inputChangeHandler}
      />
    </AmountStyled>
  );
};

export default Amount;
