export interface IExchangeRateResultModel {
  result: string;
  base_code: string;
  documentation: string;
  terms_of_use: string;
  time_last_update_unix: number;
  time_last_update_utc: string;
  time_next_update_unix: number;
  time_next_update_utc: string;
  conversion_rates: IConversionRatesModel;
}
export interface IConversionRatesModel {
  [x: string]: number;
}
