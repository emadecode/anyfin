import React, {useEffect, useState} from 'react';
import {useQuery} from 'react-query';
import {CreateQuery} from '../../core/lib/react-query';
import CountriesList from './countries-list/CountriesList';
import {IExchangeRateResultModel} from './CountrySearch.models';
import SearchBox from './search-box/SearchBox';
import {useDispatch, useSelector} from 'react-redux';
import {addRatesList} from '../../redux/ducks/exchange-rates';
import {CountriesSearchStyled} from './CountrySearch.styled';
import Amount from './amount/Amount';

import {RootState} from '../../redux/ducks/reducer';
import {loadBookmarkData} from './../../redux/ducks/bookmarks';
import {ICountryModel} from './country-item/CountryItem.models';
import useLocalstorage from '../../hooks/useLocalstorage';

const _apiKey = process.env.REACT_APP_EXCHANGE_API_KEY;
const _bookmarksKey = process.env.REACT_APP_BOOKMARKS_KEY || '';

const CountrySearch = () => {
  const [searchTerm, setSearchTerm] = useState<string>('');
  const bookmarkList = useSelector((s: RootState) => s.bookmarks.bookmarkList);
  const {setItem, getItem} = useLocalstorage();
  const dipatch = useDispatch();
  useQuery(
    CreateQuery<IExchangeRateResultModel>(
      `https://v6.exchangerate-api.com/v6/${_apiKey}/latest/SEK`,
      {
        onSuccess: data => {
          dipatch(addRatesList(data.conversion_rates));
        },
      },
    ),
  );

  useEffect(() => {
    const savedList = getItem<Array<ICountryModel>>(_bookmarksKey, true);
    if (savedList && savedList.length) {
      dipatch(loadBookmarkData(savedList));
    }
  }, [dipatch, getItem]);

  useEffect(() => {
    setItem(_bookmarksKey, bookmarkList, true);
  }, [bookmarkList, setItem]);

  return (
    <CountriesSearchStyled>
      <SearchBox onSearch={setSearchTerm} />
      <Amount />
      <CountriesList searchTerm={searchTerm} />
    </CountriesSearchStyled>
  );
};

export default CountrySearch;
