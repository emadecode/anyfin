import React, {FC} from 'react';
import {IStyledThemeProps} from '../../styles/theme';
import styled from 'styled-components';
import NumberFormat, {NumberFormatProps} from 'react-number-format';

interface ITextInputStyledProps
  extends React.InputHTMLAttributes<HTMLInputElement>,
    IStyledThemeProps {
  hasIcon?: boolean;
}

const TextInputStyled = styled.input<ITextInputStyledProps>`
  box-shadow: 0 3px 9px -8px #000;
  min-height: 2rem;
  width: 100%;
  padding: 0.5rem;
  border: 1px solid lightgray;
  border-radius: 0.2rem;
  outline: none;
  ${({hasIcon}) =>
    hasIcon &&
    `
      padding-left: 2.5rem;
  `}
`;

const FormGroupStyled = styled.div<IStyledThemeProps>`
  position: relative;
`;

const TextInputIconStyled = styled.div<IStyledThemeProps>`
  text-align: center;
  position: absolute;
  left: 10px;
  top: 50%;
  transform: translateY(-50%);
  width: 1.2rem;
  color: ${({theme}) => theme.palette.primary.main};
`;

interface ITextInputProps extends ITextInputStyledProps {
  icon?: React.ReactNode;
}

const TextInput: FC<ITextInputProps> = ({icon, ...rest}) => {
  return (
    <FormGroupStyled>
      {icon && <TextInputIconStyled>{icon}</TextInputIconStyled>}
      <TextInputStyled {...rest} hasIcon={icon ? true : false} />
    </FormGroupStyled>
  );
};

const CustomNumberFormatInputStyled = styled(NumberFormat)`
  box-shadow: 0 3px 9px -8px #000;
  min-height: 2rem;
  width: 100%;
  padding: 0.5rem;
  border: 1px solid lightgray;
  border-radius: 0.2rem;
  outline: none;
  padding-left: 2.5rem;
`;

interface ICustomNumberFormatInputProps extends NumberFormatProps<any> {
  icon?: React.ReactNode;
}

export const CustomNumberFormatInput: FC<ICustomNumberFormatInputProps> = ({
  icon,
  ...rest
}) => {
  return (
    <FormGroupStyled>
      {icon && <TextInputIconStyled>{icon}</TextInputIconStyled>}
      <CustomNumberFormatInputStyled {...rest} />
    </FormGroupStyled>
  );
};

export default TextInput;
