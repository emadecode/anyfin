import styled from 'styled-components';
import {IStyledThemeProps} from './../../../styles/theme';

export const AmountStyled = styled.div<IStyledThemeProps>`
  padding: 1rem 6rem;
`;
