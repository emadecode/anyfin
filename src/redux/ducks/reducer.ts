import {Action, combineReducers, Reducer} from '@reduxjs/toolkit';

import bookmarks from './bookmarks';
import exchangeRates from './exchange-rates';
import amount from './amount';

const appReducer = combineReducers({
  bookmarks,
  exchangeRates,
  amount,
});

const rootReducer: Reducer = (state: RootState, action: Action) => {
  return appReducer(state, action);
};

export type RootState = ReturnType<typeof appReducer>;

export default rootReducer;
