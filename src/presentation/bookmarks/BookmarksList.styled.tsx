import styled from 'styled-components';

export const BookmarksListStyled = styled.div`
  background-color: #e9e9e9;
  padding: 0 3rem;
  height: 100vh;
`;

export const BookmarksListHeaderStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 4rem;
`;
export const BookmarksListTitleStyled = styled.h3`
  font-size: 1.5rem;
`;
export const BookmarksListBodyStyled = styled.div`
  height: calc(100% - 6rem);
  padding: 2rem 1rem;
  overflow: auto;
  @media screen and (min-width: 1024px) {
    height: calc(100vh - 4rem);
  }
`;
