import {css} from 'styled-components';
import {Theme} from './theme';

export const mixins = {
  bgGradient: (theme: Theme) => {
    const color1 = theme.palette.primary.main;
    const color2 = theme.palette.primary.light;
    return css`
      background: ${color1};
      background: -webkit-gradient(
        linear,
        left top,
        right top,
        from(${color2}),
        to(${color1})
      );
      background: linear-gradient(to right, ${color2}, ${color1});
    `;
  },
};
