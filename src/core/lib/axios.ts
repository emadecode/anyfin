import axiosApi from 'axios';

// ** Used for canceling a request **
export const cancelToken = axiosApi.CancelToken;
export const isCancel = axiosApi.isCancel;

// ** Creating instance of axios **
const axios = axiosApi.create({
  baseURL: process.env.REACT_APP_BASE_API,
  headers: {
    'Content-Type': 'application/json',
  },
  timeout: 5000,
});

axios.interceptors.request.use(
  config => {
    config.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json; charset=utf8',
    };

    return config;
  },
  error => {
    Promise.reject(error);
  },
);

axios.interceptors.response.use(
  response => {
    return response;
  },
  async error => {
    return Promise.reject(error);
  },
);

export default axios;
