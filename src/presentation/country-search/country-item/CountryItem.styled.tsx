import styled from 'styled-components';
import {IStyledThemeProps} from './../../../styles/theme';

export const CountryItemStyled = styled.div`
  box-shadow: 0 3px 9px -8px #000;
  border-radius: 0.5rem;
  background-color: white;
  padding: 0.5rem;
  margin-bottom: 3rem;
`;
export const CountryItemTitleStyled = styled.div`
  position: absolute;
  top: -1.6rem;
  left: 60px;
  min-width: 5rem;
  width: -webkit-max-content;
  width: max-content;
  margin: auto;
  border: 1px solid #eaeaea;
  background: white;
  padding: 0.3rem 1.8rem;
  border-radius: 25px;
  text-align: center;
  font-size: 1rem;
  font-weight: 500;
  height: 2.2rem;
`;
interface ICountryItemSaveStyledProps extends IStyledThemeProps {
  isActive?: boolean;
}
export const CountryItemSaveStyled = styled.div<ICountryItemSaveStyledProps>`
  position: absolute;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  top: -1.6rem;
  right: 10px;
  margin: auto;
  border: 1px solid #eaeaea;
  background: white;
  border-radius: 25px;
  text-align: center;
  font-size: 1rem;
  font-weight: 500;
  height: 2.4rem;
  width: 2.4rem;
  transition: background-color 200ms ease-in-out;
  ${({isActive, theme}) =>
    isActive &&
    `
    color: #ffffff;
     background-color: ${theme.palette.success.main};
  `}
`;
export const CountryItemSaveIconStyled = styled.span<IStyledThemeProps>`
  width: 1.4rem;
`;

export const CountryItemFlagContainerStyled = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  top: -1.6rem;
  left: 0;
  margin: auto;
  border: 1px solid #eaeaea;
  background: white;
  border-radius: 6rem;
  text-align: center;
  font-size: 1rem;
  font-weight: 500;
  height: 2.2rem;
  width: 4rem;
  padding: 0.5rem 0.8rem;
`;
export const CountryItemFlagStyled = styled.img`
  width: 100%;
`;
export const CountryItemHeaderStyled = styled.div`
  position: relative;
  margin-bottom: 1rem;
`;
export const CountryItemBodyStyled = styled.div`
  padding: 1rem;
`;
export const CountryItemFooterStyled = styled.div`
  position: relative;
  padding: 0 1rem 1rem 1rem;
  display: flex;
  flex-wrap: wrap;
`;

export const CountryItemExchangeStyled = styled.div<IStyledThemeProps>`
  position: absolute;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  bottom: -1.6rem;
  left: 0;
  right: 0;
  margin: auto;
  border: 1px solid #eaeaea;
  background: #f9f9f9;
  border-radius: 0.5rem;
  text-align: center;
  font-size: 1.2rem;
  font-weight: 500;
  height: 2.2rem;
  min-width: 4rem;
  padding: 0.5rem 0.8rem;
`;

export const CountryInfoStyled = styled.div`
  display: flex;
  align-items: center;
  font-size: 1.1rem;
  font-weight: bold;
`;
export const CountryInfoTitleStyled = styled.h4`
  font-size: 1.1rem;
  font-weight: bold;
`;
export const CountryInfoValueStyled = styled.h4`
  font-size: 1rem;
  margin-bottom: 0.3rem;
  margin-left: 0.5rem;
`;

export const CountryFooterItemStyled = styled.div`
  display: flex;
  align-items: center;
  font-size: 1.1rem;
  font-weight: bold;
  flex-basis: 50%;
  @media screen and (min-width: 1024px) {
    flex-basis: 33.333%;
    flex: 1 0 auto;
  }
`;
export const CountryFooterItemTitleStyled = styled.h4<IStyledThemeProps>`
  font-size: 1rem;
  font-weight: medium;
  margin-right: 0.5rem;
  color: ${({theme}) => theme.palette.primary.main};
`;
export const CountryFooterItemValueStyled = styled.h4`
  font-size: 0.9rem;
`;
