import React, {ChangeEvent, FC, useRef} from 'react';
import TextInput from '../../../shared/text-input/TextInput';
import {SearchBoxStyled} from './SearchBox.styled';
import {Search} from '@styled-icons/bootstrap';

interface ISearchBoxProps {
  onSearch: (value: string) => void;
}

const SearchBox: FC<ISearchBoxProps> = ({onSearch}) => {
  const timeoutRef = useRef<NodeJS.Timeout | undefined>(undefined);

  const inputChangeHandler = (event: ChangeEvent<HTMLInputElement>): void => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
    timeoutRef.current = setTimeout(() => {
      onSearch(event.target.value);
    }, 1000);
  };

  return (
    <SearchBoxStyled>
      <TextInput
        data-cy="searchInput"
        onChange={inputChangeHandler}
        placeholder="Search Country"
        icon={<Search size={20} />}
      />
    </SearchBoxStyled>
  );
};

export default SearchBox;
