import {createGlobalStyle} from 'styled-components';
import {Theme} from './theme';
import {defaultFont} from './fonts';

export type IPropsModel = {
  theme: Theme;
};

const GlobalStyle = createGlobalStyle`
          ${defaultFont}
          * {
            font-family: 'Roboto', sans-serif;;
          }
          p {
            margin-top: 0;
            margin-bottom: 1rem;
          }

          .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
          margin-bottom: 0.5rem;
          font-family: inherit;
          font-weight: 500;
          line-height: 1.2;
          color: inherit;
          }
          input::-webkit-outer-spin-button,
          input::-webkit-inner-spin-button {
              -webkit-appearance: none;
              margin: 0;
          }
          input[type=number] {
              -moz-appearance:textfield;
          }
          html {
            font-size: 62.5%;
            @media only screen and (min-width: 720px) {
              font-size: 87.5%;
            }
          }
          body {
            font-family: 'Roboto', sans-serif;
            background-color: #F3F3F3;
          }
          ::-webkit-scrollbar {
            width: 1rem;
            height: 0.5rem;
          }

          /* Track */
          ::-webkit-scrollbar-track {
            background: #f1f1f1;
          }

          /* Handle */
          ::-webkit-scrollbar-thumb {
            background-color: #a8bbbf;
            border-radius: 3rem;
          }

          /* Handle on hover */
          ::-webkit-scrollbar-thumb:hover {
            background-color: #9eb2b7;
          }
`;

export default GlobalStyle;
