import {createAction, createReducer} from '@reduxjs/toolkit';

const CHANGE_AMOUNT = 'anyfin/amount/CHANGE_AMOUNT';

export const changeAmount = createAction<number | undefined>(CHANGE_AMOUNT);

interface IStateModel {
  amount: number | undefined;
}
const initialState: IStateModel = {
  amount: 1,
};

export default createReducer<IStateModel>(initialState, builder => {
  builder.addCase(changeAmount, (state, action) => {
    state.amount = action.payload;
  });
});
