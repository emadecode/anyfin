export const defaultColors = {
  primary: {
    main: '#ff7f00',
    light: '#ff7f00',
    dark: '#082032',
  },
  secondary: {
    main: '#ef4136',
    light: '#ef4136',
    dark: '#2C394B',
  },
  error: {
    main: '#D8000C',
    light: '#D8000C',
    dark: '#D8000C',
  },
  success: {
    main: '#22a958',
    light: '#4ac97c',
    dark: '#1c8746',
  },
  background: {
    default: '#F3F3F3',
  },
};
