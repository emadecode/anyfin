import React, {FC} from 'react';
import NumberFormat from 'react-number-format';
import {ICountryModel} from './CountryItem.models';
import {Bookmark, BookmarkCheckFill} from '@styled-icons/bootstrap';
import {
  CountryItemStyled,
  CountryItemHeaderStyled,
  CountryItemTitleStyled,
  CountryItemBodyStyled,
  CountryItemFlagStyled,
  CountryItemFlagContainerStyled,
  CountryInfoTitleStyled,
  CountryInfoValueStyled,
  CountryInfoStyled,
  CountryItemFooterStyled,
  CountryFooterItemStyled,
  CountryFooterItemTitleStyled,
  CountryFooterItemValueStyled,
  CountryItemSaveStyled,
  CountryItemExchangeStyled,
  CountryItemSaveIconStyled,
} from './CountryItem.styled';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../../redux/ducks/reducer';
import {removeBookmarkItem} from '../../../redux/ducks/bookmarks';
import {addBookmarkItem} from './../../../redux/ducks/bookmarks';

interface ICountryItemProps {
  data: ICountryModel;
}

const CountryItem: FC<ICountryItemProps> = ({data}) => {
  const bookmarkList = useSelector((s: RootState) => s.bookmarks.bookmarkList);
  const exchangeRatesList = useSelector(
    (s: RootState) => s.exchangeRates.ratesList,
  );
  const amount = useSelector((s: RootState) => s.amount.amount);
  const dispatch = useDispatch();

  const renderCurrencies = () => {
    return Object.keys(data.currencies).map((key, idx) => {
      return (
        <span key={key}>
          {idx > 0 && ', '} {data.currencies[key].name}(
          {data.currencies[key].symbol})
        </span>
      );
    });
  };

  const renderCurrencyCode = () => {
    return Object.keys(data.currencies).map((key, idx) => {
      return (
        <span key={idx + key}>
          {idx > 0 && ', '} {key}
        </span>
      );
    });
  };

  const renderExchangedNumber = () => {
    return Object.keys(data.currencies).map((key, idx) => {
      return (
        <span key={idx + key} data-cy="currencyAmount">
          {idx > 0 && ', '}
          {returnExchangedNumber(key) ? (
            <NumberFormat
              value={returnExchangedNumber(key)}
              thousandSeparator={true}
              displayType={'text'}
              prefix={data.currencies[key].symbol}
            />
          ) : (
            '-'
          )}
        </span>
      );
    });
  };

  const returnExchangedNumber = (key: string): number | null => {
    const exchangeRate = exchangeRatesList[key];
    return exchangeRate && amount ? exchangeRate * amount : null;
  };

  const isBookmarked = (): boolean => {
    const bookmarkedItem = bookmarkList.find(item => item.ccn3 === data.ccn3);
    return bookmarkedItem ? true : false;
  };

  const toggleBookmark = (): void => {
    isBookmarked()
      ? dispatch(removeBookmarkItem(data))
      : dispatch(addBookmarkItem(data));
  };

  return (
    <CountryItemStyled data-cy="countryItem">
      <CountryItemHeaderStyled>
        <CountryItemFlagContainerStyled>
          <CountryItemFlagStyled src={data.flags.svg} />
        </CountryItemFlagContainerStyled>

        <CountryItemTitleStyled>{data.name.common}</CountryItemTitleStyled>
        <CountryItemSaveStyled
          onClick={toggleBookmark}
          isActive={isBookmarked()}
          data-cy="bookmarkButton"
        >
          <CountryItemSaveIconStyled>
            {isBookmarked() ? (
              <BookmarkCheckFill size={18} />
            ) : (
              <Bookmark size={18} />
            )}
          </CountryItemSaveIconStyled>
        </CountryItemSaveStyled>
      </CountryItemHeaderStyled>

      <CountryItemBodyStyled>
        <CountryInfoStyled>
          <CountryInfoTitleStyled>Official Name:</CountryInfoTitleStyled>
          <CountryInfoValueStyled>{data.name.official}</CountryInfoValueStyled>
        </CountryInfoStyled>
        <CountryInfoStyled>
          <CountryInfoTitleStyled>Currency:</CountryInfoTitleStyled>
          <CountryInfoValueStyled>
            {data.currencies && renderCurrencies()}
          </CountryInfoValueStyled>
        </CountryInfoStyled>
      </CountryItemBodyStyled>
      <CountryItemFooterStyled>
        <CountryFooterItemStyled>
          <CountryFooterItemTitleStyled>Capital:</CountryFooterItemTitleStyled>
          <CountryFooterItemValueStyled>
            {data.capital && data.capital[0]}
          </CountryFooterItemValueStyled>
        </CountryFooterItemStyled>
        <CountryFooterItemStyled>
          <CountryFooterItemTitleStyled>
            Population:
          </CountryFooterItemTitleStyled>
          <CountryFooterItemValueStyled>
            <NumberFormat
              value={data.population}
              thousandSeparator={true}
              displayType={'text'}
            />
          </CountryFooterItemValueStyled>
        </CountryFooterItemStyled>
        <CountryFooterItemStyled>
          <CountryFooterItemTitleStyled>
            Currency code:
          </CountryFooterItemTitleStyled>
          <CountryFooterItemValueStyled>
            {data.currencies && renderCurrencyCode()}
          </CountryFooterItemValueStyled>
        </CountryFooterItemStyled>

        <CountryItemExchangeStyled>
          {data.currencies && renderExchangedNumber()}
        </CountryItemExchangeStyled>
      </CountryItemFooterStyled>
    </CountryItemStyled>
  );
};

export default CountryItem;
