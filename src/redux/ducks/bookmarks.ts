import {createAction, createReducer} from '@reduxjs/toolkit';
import {ICountryModel} from './../../presentation/country-search/country-item/CountryItem.models';

const ADD_BOOKMARK_ITEM = 'anyfin/bookmarks/ADD_BOOKMARK_ITEM';
const REMOVE_BOOKMARK_ITEM = 'anyfin/bookmarks/REMOVE_BOOKMARK_ITEM';
const LOAD_BOOKMARK_DATA = 'anyfin/bookmarks/LOAD_BOOKMARK_DATA';

export const addBookmarkItem = createAction<ICountryModel>(ADD_BOOKMARK_ITEM);
export const removeBookmarkItem =
  createAction<ICountryModel>(REMOVE_BOOKMARK_ITEM);
export const loadBookmarkData =
  createAction<Array<ICountryModel>>(LOAD_BOOKMARK_DATA);

interface IStateModel {
  bookmarkList: Array<ICountryModel>;
}
const initialState: IStateModel = {
  bookmarkList: [],
};

export default createReducer<IStateModel>(initialState, builder => {
  builder
    .addCase(addBookmarkItem, (state, action) => {
      state.bookmarkList = [...state.bookmarkList, action.payload];
    })
    .addCase(removeBookmarkItem, (state, action) => {
      state.bookmarkList = state.bookmarkList.filter(
        item => item.ccn3 !== action.payload.ccn3,
      );
    })
    .addCase(loadBookmarkData, (state, action) => {
      state.bookmarkList = [...action.payload];
    });
});
