import {createAction, createReducer} from '@reduxjs/toolkit';
import {IConversionRatesModel} from '../../presentation/country-search/CountrySearch.models';

const ADD_RATES_LIST = 'anyfin/exchangeRates/ADD_RATES_LIST';

export const addRatesList = createAction<IConversionRatesModel>(ADD_RATES_LIST);

interface IStateModel {
  ratesList: IConversionRatesModel;
}
const initialState: IStateModel = {
  ratesList: {},
};

export default createReducer<IStateModel>(initialState, builder => {
  builder.addCase(addRatesList, (state, action) => {
    state.ratesList = {...action.payload};
  });
});
