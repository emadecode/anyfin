import styled from 'styled-components';

export const CountriesSearchStyled = styled.div`
  width: 80%;
  height: 100vh;
  margin: auto;
  overflow: hidden;
`;
