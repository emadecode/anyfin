import React from 'react';
import {useSelector} from 'react-redux';
import {RootState} from '../../redux/ducks/reducer';
import TextInput from '../../shared/text-input/TextInput';
import CountryItem from '../country-search/country-item/CountryItem';
import {
  BookmarksListBodyStyled,
  BookmarksListHeaderStyled,
  BookmarksListStyled,
  BookmarksListTitleStyled,
} from './BookmarksList.styled';

const BookmarksList = () => {
  const bookmarkList = useSelector((s: RootState) => s.bookmarks.bookmarkList);

  const renderBookmarsList = () => {
    return bookmarkList.map(item => (
      <CountryItem data={item} key={item.ccn3} />
    ));
  };

  return (
    <BookmarksListStyled data-cy="bookmarksList">
      <BookmarksListHeaderStyled>
        <BookmarksListTitleStyled>Bookmarks List</BookmarksListTitleStyled>
      </BookmarksListHeaderStyled>
      <BookmarksListBodyStyled>{renderBookmarsList()}</BookmarksListBodyStyled>
    </BookmarksListStyled>
  );
};

export default BookmarksList;
