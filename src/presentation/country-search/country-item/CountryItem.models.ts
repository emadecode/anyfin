export interface ICountryModel {
  name: ICountryNameModel;
  flag: string;
  flags: ICountryFlagsModel;
  capital: Array<string>;
  population: number;
  currencies: ICurrenciesModel;
  ccn3: number;
}

interface ICountryFlagsModel {
  png: string;
  svg: string;
}
interface ICountryNameModel {
  common: string;
  official: string;
}
interface ICurrenciesModel {
  [x: string]: ICurrencyModel;
}
interface ICurrencyModel {
  name: string;
  symbol: string;
}
