import React, {FC} from 'react';
import {useQuery} from 'react-query';
import {CreateQuery} from '../../../core/lib/react-query';
import CountryItem from '../country-item/CountryItem';
import {ICountryModel} from '../country-item/CountryItem.models';

import {
  CountriesListStyled,
  NotFountStyled,
  LoadingStyled,
} from './CountriesList.styled';

interface ICountryListProps {
  searchTerm: string;
}

const CountriesList: FC<ICountryListProps> = ({searchTerm}) => {
  const request = useQuery(
    CreateQuery<Array<ICountryModel>>(
      `https://restcountries.com/v3.1/${
        searchTerm ? 'name/' + searchTerm : 'all'
      }`,
    ),
  );

  if (request.error) {
    return <NotFountStyled data-cy="notFound">Not Found</NotFountStyled>;
  }

  if (request.isLoading) {
    return <LoadingStyled data-cy="loading">Is loading...</LoadingStyled>;
  }

  const renderCountriesList = (data: Array<ICountryModel>) => {
    return data.map((item, idx) => {
      return <CountryItem key={idx} data={item} />;
    });
  };

  return (
    <CountriesListStyled data-cy="countriesList">
      {request.data && renderCountriesList(request.data)}
    </CountriesListStyled>
  );
};

export default CountriesList;
