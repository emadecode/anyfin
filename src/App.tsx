import React from 'react';
import {QueryClient, QueryClientProvider} from 'react-query';
import styles from './App.module.scss';
import BookmarksList from './presentation/bookmarks/BookmarksList';
import CountrySearch from './presentation/country-search/CountrySearch';
import Container from './shared/container/Container';

function App() {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
        refetchOnMount: true,
        refetchOnReconnect: true,
        retry: false,
        staleTime: 5 * 60 * 1000,
      },
    },
  });

  return (
    <Container>
      <QueryClientProvider client={queryClient}>
        <div className={styles.home}>
          <div className={styles.home__left}>
            <CountrySearch />
          </div>
          <div className={styles.home__right}>
            <BookmarksList />
          </div>
        </div>
      </QueryClientProvider>
    </Container>
  );
}

export default App;
