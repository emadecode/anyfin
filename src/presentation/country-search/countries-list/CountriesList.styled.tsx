import styled from 'styled-components';

export const CountriesListStyled = styled.div`
  height: calc(100% - 10rem);
  margin: auto;
  padding: 2rem 0.5rem;
  overflow: auto;
`;

export const CountriesListHeaderStyled = styled.div``;
export const CountriesListbodyStyled = styled.div``;

export const NotFountStyled = styled.div`
  text-align: center;
  background-color: #e1e1e1;
  padding: 2rem 0.5rem;
  border-radius: 0.5rem;
`;

export const LoadingStyled = styled.div`
  text-align: center;
  background-color: #e1e1e1;
  padding: 2rem 0.5rem;
  border-radius: 0.5rem;
`;
