import {configureStore} from '@reduxjs/toolkit';
import reducer from './ducks/reducer';

export const store = configureStore({
  reducer: reducer,
  devTools: true,
  middleware: getDefaultMiddleware => getDefaultMiddleware(),
});

export type AppDispatch = typeof store.dispatch;
