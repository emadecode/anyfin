import React, {FC} from 'react';
import {IStyledThemeProps} from '../../styles/theme';
import styled from 'styled-components';

interface ICustomButtonStyledProps
  extends React.HTMLAttributes<HTMLDivElement>,
    IStyledThemeProps {}

const ContainerStyled = styled.div<ICustomButtonStyledProps>`
  margin-right: auto;
  margin-left: auto;
`;

const Container: FC<ICustomButtonStyledProps> = ({children, ...rest}) => {
  return <ContainerStyled {...rest}>{children}</ContainerStyled>;
};

export default Container;
